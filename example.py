#!/usr/bin/env python
# coding: utf-8

# # <center> VQE for Hydrogen Molecule: Ansatz State Constructed by Sampling from a Boson Sampler <center/>
# ## <center> Comparison with Traditional State Preparation <center/>
#     
# In this tutorial we are going to exploit a boson sampler to construct ansatze for ground state of Hydrogen molecule and feed it to a Variational Quantum Eigensolver (VQE). We will model a noisy version of the algorithm and compare to the actual QPU results.

# Let's first import the necessary packages
# 
# ## Python imports
# 

# In[3]:


import numpy as np
from scipy.optimize import minimize
import networkx as nx
import matplotlib.pyplot as plt
import warnings
import matplotlib
import matplotlib.pyplot as plt
# suppress an annoying warning message from networkx plotting
warnings.filterwarnings("ignore", category=matplotlib.cbook.mplDeprecation)


# ## Xanadu imports

# In[4]:


import gbsapps.sample as samp
import gbsapps.graph.graph_sample as g_samp

import itertools
# import hafnian
import gbsapps.graph.functions as func
import gbsapps.graph.dense_subgraph as dense

import strawberryfields as sf
from strawberryfields.ops import *


# ## Forest imports

# In[5]:


from pyquil import Program, get_qc
from pyquil.gates import *
from pyquil.paulis import PauliSum, PauliTerm
from pyquil.paulis import *
from pyquil.unitary_tools import lifted_pauli

from grove.alpha.arbitrary_state.arbitrary_state import create_arbitrary_state
from grove.pyvqe.vqe import VQE

from pyquil.quil import Program
import pyquil.api as api
qvm = api.QVMConnection()

from pyquil.paulis import exponentiate


# ## other imports

# In[19]:


# from sfopenboson.ops import BoseHubbardPropagation
# from sfopenboson.ops import GaussianPropagation

from openfermion.hamiltonians import bose_hubbard
from openfermion.ops import QuadOperator
from openfermion.utils import commutator, normal_ordered
from openfermion.hamiltonians import MolecularData
from openfermion.transforms import get_fermion_operator, jordan_wigner
from openfermion.ops import QubitOperator

from forestopenfermion import pyquilpauli_to_qubitop, qubitop_to_pyquilpauli

from openfermionpyscf import run_pyscf


# ## Start the problem
# 
# Below is the picture of the working principle of the VQE algorithm. The algorithm finds the ground state energy of the system specified by the Hamiltonian in the quantum circuit.
# 
# <img src=figures\VQE.png width=600px;/>
# <center> _image taken from arXiv:1304.3061 "**A variational eigenvalue solver on a quantum processor**"_<center/>
#     
# The optimization loop will take shorter time if the initial state is chosen to be similar to the true ground state.
# Of course it is not obvious how to find a good ansatz for the initial state. 

# ## <center>Hamiltonians and States<center/>

# In[7]:


from openfermion.hamiltonians import fermi_hubbard
from openfermion.hamiltonians import jellium_model
from openfermion.utils import Grid


# In[8]:


# Bose Hubbard
#x_dimension, y_dimension, tunneling, interaction = 1, 2, 1, 1.5
#bose_hubbard_H = bose_hubbard(x_dimension, y_dimension, tunneling, interaction)

# Fermi Hubbard
#Fermi_Hubbard_H = QubitOperator('X0 Z1 Y3 ', 0.5) + QubitOperator('Z3 Z4 ', 0.6)
#t = 1.0
#U = 4.0
#Fermi_Hubbard_H = fermi_hubbard(x_dimension=10, y_dimension=10, 
#                                    tunneling=t, coulomb=U,
#                                    chemical_potential=0.0, periodic=True )

#jellium (homogeneous electron gas)
#jellium_hamiltonian = jellium_model(Grid(dimensions=2, length=10, scale=1.0))

# Forced Oscillator
#forced_oscillator_H = QuadOperator('q0 q0', 0.5) + QuadOperator('p0 p0', 0.5) - QuadOperator('q0', 2)

# Ising
#initial_angle = [0.0]
#xy = PauliTerm('X', 0) * PauliTerm('Y', 1)
#hamilt_zx_xy = sZ(0) + sX(1) + xy


# Hydrogen Molecule
def h2_hamiltonian():
    """
    creates H2 hamiltonian using openfermion library
    returns a pyquil PauliTerm object
    """
    geometry = [['H', [0, 0, 0]], ['H', [0, 0, 0.74]]] # H--H distance = 0.74pm
    basis = 'sto-3g'
    multiplicity = 1 #(2S+1)
    charge = 0
    h2_molecule = MolecularData(geometry, basis, multiplicity, charge)

    h2_molecule = run_pyscf(h2_molecule)
    one_electron_integrals = h2_molecule.one_body_integrals
    two_electron_integrals = h2_molecule.two_body_integrals
    h2_qubit_hamiltonian = jordan_wigner(get_fermion_operator(h2_molecule.get_molecular_hamiltonian()))
    pyquil_h2_qubit_hamiltonian = qubitop_to_pyquilpauli(h2_qubit_hamiltonian)
    
    return pyquil_h2_qubit_hamiltonian


# ## <center> States to be used as ansatze <center/>

# In[9]:


def create_bose_hubbard_state(samples=True):
    prog = sf.Program(2)
    if samples:
        return create_arbitrary_state(samples)
    else:
        return prepare_H2_ground_state()
    eng = sf.Engine("fock", backend_options={"cutoff_dim": 3})

    with prog.context as q:
        Fock(2) | q[1]
        BoseHubbardPropagation(bose_hubbard_H, 1.086, 20) | q

    state = eng.run(prog).state

    return state



def create_forced_oscillator_state():
    # create the engine
    eng = sf.Engine("gaussian")

    prog = sf.Program(1)

    with prog.context as q:
        Xgate(1) | q[0]
        Zgate(0.5) | q[0]
        GaussianPropagation(forced_oscillator_H, t) | q

    state = eng.run(prog).state
    eng.reset()
    results[:, step] = state.means()
        
    return state



def prepare_H2_ground_state():
    """
    prepare a H2 state via pyquil
    rtype: a pyquil Program
    return: state evloved under H2 hamiltonian
    """
    #import pdb
    #pdb.set_trace()
    qvm = api.QVMConnection()
    localized_electrons_circuit = Program()

    # put two-localized electrons on the first spatial site
    localized_electrons_circuit.inst([X(0), X(1)]) 
    
    # start circuit to prepare the state
    H2_ground_state = Program()
    
    pyquil_h2_qubit_hamiltonian = h2_hamiltonian()

    #First-order Trotter evolution for t=0.1
    for term in pyquil_h2_qubit_hamiltonian.terms:
        H2_ground_state += exponentiate(0.1 * term)

    return H2_ground_state


# ## <center>State Preparation via Gaussian Boson Sampler<center/>
#     
# <img src=figures\Boson_sampler.png width=600px;/>

# In[10]:


def quantum_sampler(adj):
    """
    samples from a gaussian boson sampler
    :param adj: adjacency matrix of a graph to sample from 
    
    """
    graph = nx.Graph(adj)
    samps = 20
    n_mean = 3*samps

    quantum_samples = samp.quantum_sampler(A=adj, n_mean=n_mean, samples=samps, threshold=False)
    
    return quantum_samples


# In[11]:


def sample(m, quantum=False, hamiltonian = None):
    """
    sample either from a boson sampler or other type of samplers
    :param m: int -- number of smples 
    :param quantum: bool -- change to True to get quantum samples
    return: list of numbers drawn from the samplers
    """
    if quantum:
        if hamiltonian == 'H2':
            hamiltonian = h2_hamiltonian()
            adj = pauliSum_to_matrix(hamiltonian)
            return quantum_sampler(adj)
        else: 
            raise Exception('enter H2 as hamiltonian, we have not implemented others yet')
        
    return np.random.rand(m)


# In[12]:


#def ansatz(arb=True, samples):
def ansatz(samples):
    """
    prepares ansatze
    :param samples: list -- numbers to use as amplitudes to create a state
                            if not provided a H2 state will be prepared
    :rtype: pyquil Program
    return: prepared quantum state
    """
    #if arb:
    return create_arbitrary_state(samples)
    
    #return prepare_H2_ground_state()


# In[13]:


def pauliSum_to_matrix(pauli):
    
    """
    Takes a PauliSum object along with a list of
    qubits and returns a matrix corresponding the tensor representation of the
    object.
    Useful for generating the full Hamiltonian after a particular fermion to
    pauli transformation. 
    
    :param pauli: Pauli representation of an operator
    :param qubits: list of qubits in the order they will be represented in the resultant matrix.
    :returns: matrix representation of the pauli_sum operator
    """
    
    return lifted_pauli(pauli, [0,1,2,3])


# # <center>VQE<center/>

# ## instantiate a VQE object we imported from grove

# In[14]:


vqe_inst = VQE(minimizer=minimize,
               minimizer_kwargs={'method': 'nelder-mead'})


# ### Here we run the quantum simulator (quantum computer) in the above figure to get the energy expectations

# In[15]:



hamiltonian = h2_hamiltonian()
samples = sample(200)
angle_range = np.linspace(0.0, 2 * np.pi, 50)
data   = [vqe_inst.expectation(create_arbitrary_state(samples[x:x+4]), 
                               hamiltonian, 5000, qvm) for x in range(0, len(samples), 4)]


# ### Plot to see where the minimum might take place

# In[15]:


plt.xlabel(' \'Angle \' ')
plt.ylabel('Expectation value')
plt.plot(angle_range, data)
plt.show()


# In[ ]:


initial_params = samples
result = vqe_inst.vqe_run(ansatz, hamiltonian, initial_params, qvm=qvm)
print(result)


# ## VQE with noisy QVM

# In[ ]:


pauli_channel = [0.1, 0.1, 0.1] #10% chance of each gate at each timestep
noisy_qvm = api.QVMConnection(gate_noise=pauli_channel)


# In[ ]:


vqe_inst.minimizer_kwargs = {'method': 'Nelder-mead', 
                             'options': {'initial_simplex': np.array([[0.0], [0.05]]), 'xatol': 1.0e-2}}

result = vqe_inst.vqe_run(ansatz, hamiltonian, samples, samples=10000, qvm=noisy_qvm)
print(result)

