x_dimension, y_dimension, tunneling, interaction = 1, 2, 1, 1.5

bose_hubbard_H = bose_hubbard(x_dimension, y_dimension, tunneling, interaction)

def create_bose_hubbard_state():
    prog = sf.Program(2)
    eng = sf.Engine("fock", backend_options={"cutoff_dim": 3})

    with prog.context as q:
        Fock(2) | q[1]
        BoseHubbardPropagation(H, 1.086, 20) | q

    state = eng.run(prog).state

    return state
