# Copyright 2019 Xanadu Quantum Technologies Inc.
r"""
Graph functions
===============

**Module name:** :mod:`gbsapps.graph.functions`

.. currentmodule:: gbsapps.graph.functions

This module provides some ancillary functions for dealing with graphs. This includes
:func:`is_adjacency` to check if an input matrix is symmetric and :func:`subgraph_adjacency` to
return the adjacency matrix of a subgraph when an input graph and subset of nodes is specified.

Furthermore, the frontend :func:`~gbsapps.graph.dense_subgraph.find_dense_subgraph` function
allows users to input graphs both as a `NumPy <https://www.numpy.org/>`__ array containing the
adjacency matrix and as a `NetworkX <https://networkx.github.io/>`__ ``Graph`` object. The
:func:`to_networkx_graph` function allows both inputs to be processed into a NetworkX Graph for
ease of processing in GBSApps.

Summary
-------

.. autosummary::
    is_adjacency
    subgraph_adjacency
    to_networkx_graph

Code details
------------
"""
from typing import Union

import networkx as nx
import numpy as np

graph_type = Union[nx.Graph, np.ndarray]


def to_networkx_graph(graph: graph_type) -> nx.Graph:
    """Converts input graph into a NetworkX graph.

    Given an input graph of type ``graph_type = Union[nx.Graph, np.ndarray]``, this function
    outputs a NetworkX graph of type ``nx.Graph``. The input ``np.ndarray`` must be an adjacency
    matrix (i.e., satisfy :func:`is_adjacency`) and also real.

    Args:
        graph (graph_type): input graph to be processed

    Returns:
        graph: the NetworkX graph corresponding to the input
    """

    if isinstance(graph, np.ndarray):
        if not is_adjacency(graph) or not np.allclose(graph, graph.conj()):
            raise ValueError("Adjacency matrix must be real and symmetric")

        graph = nx.Graph(graph)

    elif not isinstance(graph, nx.Graph):
        raise TypeError("Graph is not of valid type")

    return graph


def is_adjacency(mat: np.ndarray) -> bool:
    """Checks if input is an adjacency matrix, i.e., symmetric.

    Args:
        mat (array): input matrix to be checked

    Returns:
        bool: returns ``True`` if input array is an adjacency matrix and ``False`` otherwise
    """

    if not isinstance(mat, np.ndarray):
        raise TypeError("Input matrix must be a numpy array")

    dims = mat.shape

    conditions = len(dims) == 2 and dims[0] == dims[1] and dims[0] > 1 and np.allclose(mat, mat.T)

    return conditions


def subgraph_adjacency(graph: graph_type, nodes: list) -> np.ndarray:
    """Give adjacency matrix of a subgraph

    Given a list of nodes selecting a subgraph, this function returns the corresponding adjacency
    matrix.

    Args:
        graph (graph_type): the input graph
        nodes (list): a list of nodes used to select the subgraph

    Returns:
        array: the adjacency matrix of the subgraph
    """
    graph = to_networkx_graph(graph)
    all_nodes = graph.nodes

    if not set(nodes).issubset(all_nodes):
        raise ValueError(
            "Must input a list of subgraph nodes that is contained within the nodes of the input "
            "graph"
        )

    return nx.to_numpy_array(graph.subgraph(nodes))


# TODO: Functionality for plotting graphs and subgraphs
