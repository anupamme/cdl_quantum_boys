# Copyright 2019 Xanadu Quantum Technologies Inc.
r"""
Graph sampling
==============

**Module name:** :mod:`gbsapps.graph.graph_sample`

.. currentmodule:: gbsapps.graph.graph_sample

This module provides functionality for sampling of subgraphs. The
:func:`dense_subgraph_sampler_gbs` and :func:`uniform_subgraph_sampler` functions generate raw
samples from their respective samplers in the :mod:`gbsapps.sample` module and output samples
corresponding to subgraphs of a fixed size.

Both functions use :func:`samples_to_subgraphs` to convert from raw samples to subgraphs.
However, samples from :func:`gbsapps.sample.quantum_sampler` have a variable number of clicks
(equivalently, the number of ones in the length-:math:`N` binary string). This results in
subgraphs of a random size in :func:`dense_subgraph_sampler_gbs`, which need to be resized to the
specified number of nodes. Resizing is achieved with the :func:`dense_subgraph_resize` function,
which adds or removes nodes one at a time by picking the option that results in the greatest
density.

Summary
-------

.. autosummary::
    dense_subgraph_resize
    dense_subgraph_sampler_gbs
    samples_to_subgraphs
    uniform_subgraph_sampler

Code details
------------
"""
import itertools
from typing import Union

import networkx as nx
import numpy as np

import gbsapps.sample


# pylint: disable=too-many-branches
def dense_subgraph_resize(
    subgraph_nodes: Union[list, np.ndarray], graph: nx.Graph, target_number_of_nodes: int
) -> list:
    """Resize an input subgraph to a given size.

    This function uses a greedy approach to iteratively add/remove nodes to an input subgraph.
    Suppose the input subgraph has :math:`M` nodes. When shrinking, the algorithm considers
    all :math:`M-1` node subgraphs of the input subgraph, and picks the one with the greatest
    density. It then considers all :math:`M-2` node subgraphs of the chosen :math:`M-1` node
    subgraph, and continues until the desired subgraph size has been reached.

    When growing, the algorithm considers all :math:`M+1` node subgraphs of the input graph given by
    combining the input :math:`M` node subgraph with another node from the remainder of the input
    graph, and picks the one with the greatest density. It then considers all :math:`M+2` node
    subgraphs given by combining the chosen :math:`M+1` node subgraph with nodes remaining from
    the overall graph, and continues until the desired subgraph size has been reached.

    Args:
        subgraph_nodes (list or array): a list of nodes corresponding to the subgraph to be resized
        graph (nx.Graph): the input graph
        target_number_of_nodes (int): the target number of nodes in the subgraph (must be
            larger than 1)

    Returns:
        list: a list of nodes of length ``target_number_of_nodes`` corresponding to the
        resized subgraph
    """
    # TODO: Allow randomness in greedy algorithm
    # TODO: Functionality for resizing multiple subgraphs at once
    if isinstance(target_number_of_nodes, float):
        target_number_of_nodes = int(target_number_of_nodes)
    elif not isinstance(target_number_of_nodes, int):
        raise TypeError(
            "Target number of nodes for resizing must be an integer in the form of an "
            "int or float"
        )

    if target_number_of_nodes < 2:
        raise ValueError("Must resize to subgraph of 2 or more nodes")

    if not isinstance(subgraph_nodes, (list, np.ndarray)):
        raise TypeError("Input subgraph nodes must be a list or a numpy array")

    if isinstance(subgraph_nodes, np.ndarray) and not len(subgraph_nodes.shape) == 1:
        raise ValueError("Input subgraph nodes must be a rank-1 numpy array")

    subgraph_nodes = sorted(list(subgraph_nodes))

    nodes = graph.nodes

    if not set(subgraph_nodes).issubset(nodes):
        raise ValueError(
            "Must input a list of subgraph nodes that is contained within the nodes "
            "of the input graph"
        )
    if target_number_of_nodes >= len(nodes):
        raise ValueError(
            "Must input a target number of nodes that is less than the number of "
            "nodes in the input graph"
        )

    remaining_nodes = [i for i in nodes if i not in subgraph_nodes]

    # TODO: Consider dynamic programming approach
    while len(subgraph_nodes) < target_number_of_nodes:

        new_subgraph_nodes = [subgraph_nodes + [n] for n in remaining_nodes]
        subgraph_nodes = max((nx.density(graph.subgraph(x)), x) for x in new_subgraph_nodes)[1]

        remaining_nodes = [i for i in nodes if i not in subgraph_nodes]

    while len(subgraph_nodes) > target_number_of_nodes:

        candidate_subgraphs_nodes = list(
            itertools.combinations(subgraph_nodes, len(subgraph_nodes) - 1)
        )

        subgraph_nodes = max(
            [(nx.density(graph.subgraph(n)), n) for n in candidate_subgraphs_nodes]
        )[1]

    return sorted(subgraph_nodes)


def dense_subgraph_sampler_gbs(
    graph: nx.Graph,
    number_of_nodes: int,
    samples: int = 1,
    remote: bool = False,
    backend: str = "gaussian",
) -> list:
    r"""Perform Gaussian boson sampling to select dense subgraphs of an input graph.

    This function uses samples from :func:`quantum_sampler` with threshold detectors and
    postselection on samples above ``0.75`` times the desired ``number_of_nodes``. This allows
    for subgraphs to be selected. Subgraphs are initially of variable size, but are then resized
    using :func:`dense_subgraph_resize` to the desired ``number_of_nodes``.

    Args:
        graph (nx.Graph): the input graph
        number_of_nodes (int): the size of subgraphs to be sampled
        samples (int): number of samples; defaults to 1
        remote (bool): Performs sampling on a remote server if ``True``. Remote sampling is required
            for sampling on hardware. If not specified, sampling will be performed locally.
        backend (str): requested backend for sampling; defaults to ``"gaussian"``

    Returns:
        list: a list of length ``samples`` whose elements are integer lists of length
        ``len(number_of_nodes)`` indicating which nodes of the input graph are selected
    """

    postselect = int(0.75 * number_of_nodes)

    gbs_samples = gbsapps.sample.quantum_sampler(
        A=nx.to_numpy_array(graph),
        n_mean=number_of_nodes,
        samples=samples,
        remote=remote,
        backend=backend,
        threshold=True,
        postselect=postselect,
    )

    subgraph_samples = samples_to_subgraphs(graph, gbs_samples)

    return [dense_subgraph_resize(sample, graph, number_of_nodes) for sample in subgraph_samples]


def uniform_subgraph_sampler(graph: nx.Graph, sampled_nodes: int, samples: int = 1) -> list:
    r"""Perform classical sampling of subgraphs using the uniform distribution.

    Args:
        graph (nx.Graph): the input graph
        sampled_nodes (int): the size of subgraphs to be sampled
        samples (int): number of samples; defaults to 1

    Returns:
        list: a list of length ``samples`` whose elements are length ``number_of_nodes`` lists of
        integers indicating which nodes of the input graph are selected
    """
    # TODO: Make number of sampled modes a random variable matching distribution from GBS and use
    #  greedy growth/ shrinking for a fairer comparison

    uniform_samples = gbsapps.sample.uniform_sampler(graph.order(), sampled_nodes, samples)

    return samples_to_subgraphs(graph, uniform_samples)


def samples_to_subgraphs(graph: nx.Graph, samples: list) -> list:
    """Converts a list of samples to a list of subgraphs.

    Given a list of samples, with each sample of ``len(nodes)`` being a list of zeros and ones,
    this function returns a list of subgraphs selected by, for each sample, picking the nodes
    corresponding to ones and creating the induced subgraph. The subgraph is specified as a list
    of selected nodes. For example, given an input 6-node graph, a sample :math:`[0, 1, 1, 0, 0,
    1]` is converted to a subgraph :math:`[1, 2, 5]`.

    Args:
        graph (nx.Graph): the input graph
        samples (list): a list of samples, each a binary sequence of ``len(nodes)``

    Returns:
        list: a list of subgraphs, where each subgraph is represented by a list of its nodes
    """
    # TODO: Functionality for feeding in a single sample
    graph_nodes = list(graph.nodes)
    node_number = len(graph_nodes)

    def to_subgraph(sample: list) -> list:
        """Convert a single sample to a subgraph

        Args:
           sample (list): a binary sample of ``len(nodes)``

        Returns:
            list: a subgraph specified by its nodes
        """
        return list(np.nonzero(sample)[0])

    subgraph_samples = [to_subgraph(sample) for sample in samples]

    if graph_nodes != list(range(node_number)):
        return [sorted([graph_nodes[i] for i in s]) for s in subgraph_samples]

    return subgraph_samples
