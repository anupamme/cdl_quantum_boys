# Copyright 2019 Xanadu Quantum Technologies Inc.
r"""
Dense subgraph identification
=============================

**Module name:** :mod:`gbsapps.graph.dense_subgraph`

.. currentmodule:: gbsapps.graph.dense_subgraph

The frontend module for users to find dense subgraphs. The :func:`find_dense_subgraph` function
provides approximate solutions to the densest-:math:`k` subgraph problem
:cite:`arrazola2018using`, which is NP-hard. This problem considers a graph :math:`G = (V,
E)` of :math:`N` nodes :math:`V` and a list of edges :math:`E`, and sets the objective of finding a
:math:`k`-vertex subgraph with the greatest density. In this setting, subgraphs :math:`G[S]` are
defined by nodes :math:`S \subset V` and corresponding edges :math:`E_{S} \subseteq E` that
have both endpoints in :math:`S`. The density of a subgraph is given by

.. math:: d(G[S]) = \frac{2 |E_{S}|}{|S|(|S|-1)},

where :math:`|\cdot|` denotes cardinality, and the densest-:math:`k` subgraph problem can be
written
succinctly as

.. math:: {\rm argmax}_{S \in \mathcal{S}_{k}} d(G[S])

with :math:`\mathcal{S}_{k}` the set of all possible :math:`k` node subgraphs. This problem grows
combinatorially with :math:`{N \choose k}` and is NP-hard in the worst case.

The :func:`find_dense_subgraph` function provides access to heuristic algorithms for finding
approximate solutions. At present, random search is the heuristic algorithm provided, accessible
through the :func:`random_search` function. This algorithm proceeds by randomly generating a set
of :math:`k` vertex subgraphs and selecting the densest. Sampling of subgraphs can be achieved
both uniformly at random and also with a quantum sampler programmed to be biased toward
outputting dense subgraphs.

Summary
-------

.. autosummary::
    METHOD_DICT
    find_dense_subgraph
    random_search

Code details
------------
"""
import functools
from typing import Callable, Union, Tuple
import networkx as nx

from gbsapps.graph.graph_sample import dense_subgraph_sampler_gbs, uniform_subgraph_sampler
from gbsapps.graph.functions import to_networkx_graph, graph_type


# pylint: disable=too-many-arguments
def find_dense_subgraph(
    graph: graph_type,
    number_of_nodes: int,
    iterations: int = 1,
    method: Union[str, Callable] = "random-search",
    remote: bool = False,
    backend: str = "gaussian",
) -> Tuple[float, list]:
    """Functionality for finding dense `node-induced subgraphs
    <http://mathworld.wolfram.com/Vertex-InducedSubgraph.html>`__ of a given size.

    The user is able to specify a stochastic algorithm as a method for optimization. Furthermore,
    the form of randomness used (e.g., GBS sampling or uniform sampling) in the stochastic
    algorithm can also be specified by setting a backend sampler. This sampling can be performed
    locally or remotely.

    Methods are set with the ``methods`` argument. The available methods are:

    - ``"random-search"``: a simple random search algorithm where many subgraphs are selected and
      the densest one is chosen (default).

    Backends are set with the ``backend`` argument. They include the quantum backends
    given in :meth:`gbsapps.sample.QUANTUM_BACKENDS` as well as a ``"uniform"`` sampler backend.
    The available backends are:

    - ``"gaussian"``: allowing subgraph samples to be simulated using the Gaussian backend of Strawberry Fields (default)
    - ``"uniform"``: allowing subgraph samples to be generated with a uniform distribution using
      the :func:`~gbsapps.graph.graph_sample.uniform_subgraph_sampler` function

    Args:
        graph (graph_type): the input graph
        number_of_nodes (int): the size of desired dense subgraph
        iterations (int): number of iterations to use in algorithm
        method (str or function): either a string selecting from a range of available methods or a
            customized callable function. Defaults to ``"random-search"``.
        remote (bool): Performs sampling on a remote server if ``True``. Remote sampling is required
            for sampling on hardware and is not available with the ``"uniform"`` backend. If not
            specified, sampling will be performed
            locally.
        backend (str): requested backend for sampling; defaults to the ``"gaussian"`` backend

    Returns:
        (float, list): the density and list of nodes corresponding to the densest subgraph found
    """
    if backend == "uniform":
        sampler = uniform_subgraph_sampler
    else:
        sampler = functools.partial(dense_subgraph_sampler_gbs, remote=remote, backend=backend)

    if not callable(method):
        if method in METHOD_DICT:
            method = METHOD_DICT[method]
        else:
            raise Exception("Optimization method must be callable or a valid string")

    return method(to_networkx_graph(graph), number_of_nodes, sampler, iterations=iterations)


def random_search(
    graph: nx.Graph, number_of_nodes: int, sampler: Callable, iterations: int = 1
) -> Tuple[float, list]:
    """Random search algorithm for finding dense subgraphs of a given size.

    The algorithm proceeds by sampling subgraphs according to the function ``sampler``. The
    sampled subgraphs are of size ``number_of_nodes``. The densest subgraph is then selected
    among all the samples.

    Args:
        graph (nx.Graph): the input graph
        number_of_nodes (int): the size of desired dense subgraph
        sampler (function): a function which returns a given number of samples of subgraphs of a
            given size. Must accept arguments in the form: ``(graph: nx.Graph, sampled_nodes: int,
            samples: int = 1)``.
        iterations (int): number of iterations to use in algorithm

    Returns:
        (float, list): the density and list of nodes corresponding to the densest subgraph found
    """
    output_samples = sampler(graph, number_of_nodes, iterations)

    density_and_samples = [(nx.density(graph.subgraph(s)), s) for s in output_samples]

    return max(density_and_samples)


METHOD_DICT = {"random-search": random_search}
"""Dict[str, func]: Included methods for finding dense subgraphs. The dictionary keys are strings
describing the method, while the dictionary values are callable functions corresponding to the
method."""
