# Copyright 2019 Xanadu Quantum Technologies Inc.
r"""
Init
====
"""
from ._version import __version__


def version():
    """Gives the package version.

    Returns:
        string: package version number

    """
    return __version__
