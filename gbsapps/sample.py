# Copyright 2019 Xanadu Quantum Technologies Inc.
r"""
Sampling functions
==================

**Module name:** :mod:`gbsapps.sample`

.. currentmodule:: gbsapps.sample

This module provides functionality for generating samples from both a quantum device and the
uniform distribution.

The :func:`quantum_sampler` function allows users to simulate Gaussian boson sampling (GBS) by
choosing a symmetric input matrix to sample from. For a symmetric :math:`N \times N` input
matrix :math:`A`, an :math:`N`-mode GBS device with threshold detectors generates samples that are
binary strings of length ``N``. Various problems can be encoded in the matrix :math:`A` so that the
output samples are informative and can be used as a form of randomness to improve solvers,
as outlined in Refs. :cite:`arrazola2018using` and :cite:`arrazola2018quantum` and also shown in
our :ref:`tutorial_dense_subgraph` tutorial.

On the other hand, the :func:`uniform_sampler` function allows users to generate samples where a subset of
modes are selected using the uniform distribution.

Summary
-------

.. autosummary::
    QUANTUM_BACKENDS
    quantum_sampler
    uniform_sampler

Code details
------------
"""
import numpy as np
import strawberryfields as sf
import gbsapps.graph.functions

QUANTUM_BACKENDS = ("gaussian",)
"""tuple[str]: Available quantum backends for sampling."""


def _valid_backend(backend: str) -> bool:
    r"""Performs a validation check that the requested backend can be run based upon the
    ``QUANTUM_BACKENDS`` tuple.

    Args:
        backend (str): requested backend for sampling

    Returns:
        bool: returns only ``True`` if a valid backend is selected
    """
    return backend in QUANTUM_BACKENDS


# pylint: disable=too-many-arguments,expression-not-assigned,pointless-statement
def quantum_sampler(
    A: np.ndarray,
    n_mean: float,
    samples: int = 1,
    remote: bool = False,
    backend: str = "gaussian",
    threshold: bool = True,
    postselect: int = 0,
) -> list:
    r"""Perform quantum sampling of adjacency matrix :math:`A` using the Gaussian boson sampling
    algorithm.

    Quantum sampling is performed using a backend specified by the ``backend`` argument. Available
    backends are listed in ``QUANTUM_BACKENDS``. These are:

    - ``"gaussian"``: for simulating the output of a GBS device using the Gaussian backend of `Strawberry Fields
      <https://strawberryfields.readthedocs.io/en/latest/>`__.

    Args:
        A (array): the (real or complex) :math:`N \times N` adjacency matrix to sample from
        n_mean (float): mean photon number
        samples (int): number of samples; defaults to 1
        remote (bool): performs sampling on a remote server if ``True``. Remote sampling is required
            for sampling on hardware. If not specified, sampling will be performed locally.
        backend (str): requested backend for sampling; defaults to ``"gaussian"``
        threshold (bool): Performs sampling using threshold (on/off click) detectors if ``True`` or
            using photon number resolving detectors if ``False``. Defaults to ``True`` for threshold
            sampling.
        postselect (int): Causes samples with a photon number or click number less than the
            specified value to be filtered out. Defaults to ``0``, resulting in no postselection.
            Note that the number of samples returned is still equal to ``samples``.

    Returns:
        list: a list of length ``samples`` whose elements are length :math:`N` lists of
        integers indicating events (e.g., photon numbers or clicks) in each of the :math:`N` modes
        of the detector
    """
    # TODO: Warnings for when n_mean and postselect are too high

    if not gbsapps.graph.functions.is_adjacency(A):
        raise ValueError("Input must be a NumPy array corresponding to a symmetric matrix")

    if samples < 1:
        raise ValueError("Number of samples must be at least one")

    nodes = len(A)
    p = sf.Program(nodes)

    mean_photon_per_mode = n_mean / float(nodes)

    with p.context as q:
        sf.ops.GraphEmbed(A, mean_photon_per_mode=mean_photon_per_mode) | q
        sf.ops.Measure | q

    p = p.compile("gbs")

    if postselect == 0:
        s = _sample_sf(p, shots=samples, remote=remote, backend=backend)

        if threshold:
            s[s >= 1] = 1

        s = s.tolist()

    elif postselect > 0:
        s = []

        while len(s) < samples:
            samp = np.array(_sample_sf(p, shots=1, remote=remote, backend=backend))

            if threshold:
                samp[samp >= 1] = 1

            counts = np.sum(samp)

            if counts >= postselect:
                s.append(samp.tolist())

    else:
        raise ValueError("Can only postselect on nonnegative values")

    return s


def _sample_sf(
    p: sf.Program, shots: int = 1, remote: bool = False, backend: str = "Gaussian"
) -> np.ndarray:
    """Generate samples from Strawberry Fields

    Args:
        p (sf.Program): the program to sample from
        shots (int): the number of samples; defaults to 1
        remote (bool): Performs sampling on a remote server if ``True``. Remote sampling is required
            for sampling on hardware. If not specified, sampling will be performed locally.
        backend (str): requested backend for sampling; defaults to ``"gaussian"``

    Returns:
        array: an array of ``len(shots)`` samples, with each sample the result of running the
        Strawberry Fields program ``p``

    """
    if not _valid_backend(backend):
        raise ValueError("Invalid backend selected")

    if remote:
        # TODO: Communicating with remote server
        raise ValueError("Remote sampling not yet available")

    eng = sf.LocalEngine(backend=backend)

    return np.array(eng.run(p, run_options={"shots": shots}).samples)


def uniform_sampler(modes: int, sampled_modes: int, samples: int = 1) -> list:
    """Perform classical sampling using the uniform distribution to randomly select a subset of
    modes of a given size.

    Args:
        modes (int): number of modes to sample from
        sampled_modes (int): number of modes to sample
        samples (int): number of samples; defaults to 1

    Returns:
        list: a ``len(samples)`` list whose elements are ``len(modes)`` lists which contain
        zeros and ones. The number of ones is ``samples_modes``, corresponding to the modes
        selected.
    """

    if modes < 1 or sampled_modes < 1 or sampled_modes > modes:
        raise ValueError(
            "Modes and sampled_modes must be greater than zero and sampled_modes must not exceed "
            "modes "
        )
    if samples < 1:
        raise ValueError("Number of samples must be greater than zero")

    base_sample = [1] * sampled_modes + [0] * (modes - sampled_modes)

    output_samples = [list(np.random.permutation(base_sample)) for _ in range(samples)]

    return output_samples
