from sfopenboson.ops import BoseHubbardPropagation
from sfopenboson.ops import GaussianPropagation

from openfermion.hamiltonians import bose_hubbard
from openfermion.ops import QuadOperator
from openfermion.utils import commutator, normal_ordered
from openfermion.hamiltonians import MolecularData
from openfermion.transforms import get_fermion_operator, jordan_wigner
from openfermion.ops import QubitOperator

from forestopenfermion import pyquilpauli_to_qubitop, qubitop_to_pyquilpauli

from openfermionpyscf import run_pyscf



#def bose_hubbard(x_dimension, y_dimension, tunneling, interaction):
#	return bose_hubbard(x_dimension, y_dimension, tunneling, interaction)

def create_bose_hubbard_state():
    prog = sf.Program(2)
    eng = sf.Engine("fock", backend_options={"cutoff_dim": 3})

    with prog.context as q:
        Fock(2) | q[1]
        BoseHubbardPropagation(bose_hubbard, 1.086, 20) | q

    state = eng.run(prog).state

    return state
